<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'dinamic');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zojZ280Uve&=&mC{pm}>A)tJj15)3])ET%esiei;q<5^)j3Q..yw$W*jy;@lh%q#');
define('SECURE_AUTH_KEY',  '.>{9h(cJ8-)@?lqz=megH{$(IQ&JofTV1; G(X@ej2,[$(90]A; Obg{dvC|b<`%');
define('LOGGED_IN_KEY',    'd0;j5>u+o8=OR@CKXEbB#ittaG%RV{:(T><j7xVz4DM`f|&3z9,iBhV9a0e3Q(wX');
define('NONCE_KEY',        'yH]a[KB;5ICB*%0V0s1Jd)MHOh}}9)!:&{j$lZ!1E+U|cmkP_]FdNt~?mug/}>t2');
define('AUTH_SALT',        '67GKN3u05Na{s,b.ljSY5M#Y#<~M9+rb~4p_(q7L7fbJE:T|74f7j72@%f#4giCU');
define('SECURE_AUTH_SALT', 'D&<H(!hE)->W`3}#LJ#rr-81|?!^>kl/zqJq70uofuF*Z}1zbQ!#D|B5cf|:*>40');
define('LOGGED_IN_SALT',   '[I*/f?Ew;bJ^2~Qks<aT1~E^J{/wdsh]l5*v/4`KC4Tm.PVI1${)yW5n*K^9myGt');
define('NONCE_SALT',       '4.*y;aH#UFn`tw{><,4a|&tbXgqV}- _l<S;-0iyS3h KZH8;hP#x]N<hKaeC+7I');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'dn_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
