<?php 
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

get_header();?>
<?php if ( have_posts() ) : the_post(); ?>
    <header style="background-image: url(<?php echo IMAGES; ?>/bg-clientes.jpg);">

    </header>
    <main>
        <section id="noticia">
            <div class="title">
                <h2 class="verde">Fique por dentro de nossas noticias</h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col l9 m9 s12">
                        <article class="col l12 m12 s12 post">
                            <div class="post-body">
                                <div class="thumb" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
                                    <div class="overlay">
                                        <date><?php $dia = strtoupper(the_time('l'));  echo  $dia;?> | <?php echo the_time('d/m/Y');?></date> | 
                                        <div class="category">
                                            <?php 
                                            $terms = get_the_terms(get_the_id(),'categoria-noticia');
                                            $catArray = array();
                                            if ($terms != '') :
                                                    foreach ($terms as $term) { echo $term->name . " • ";  array_push($catArray, $term->slug);
                                            } 
                                                else : echo ""; endif;?>
                                        </div>
                                    </div>
                                </div>
                                <div class="text">
                                    <h3><?php echo get_the_title();?></h3>
                                    <p><?php echo the_content();?></p>
                                    <?php 
                                    $terms = get_the_terms(get_the_id(),'tag-noticias');
                                    if ( $terms == "") :
                                    echo "<p><b>Tags: </b> </p>";

                                    else : ?>


                                    <p><b>Tags: </b><?php 
                                    if ($terms != '') :
                                            foreach ($terms as $term) { echo $term->name . " • ";
                                    } 
                                    else : echo ""; endif;?> </p>
                                <?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </div>
                    <aside class="col l3 m3 s12">
                        <span>Leia Também</span>
                        <ul class="posts">
                            <?php $args = array(
                                'post_type'   => 'noticias',
                                'post_status' => 'publish',
                                'categoria-noticia' => $catArray,
                                'posts_per_page' => 4,
                                );
                            $query = new WP_Query( $args );

                            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();?>

                            <li><a href="<?php the_permalink(); ?>"><?php the_title();?></a></li>

                            <?php endwhile; else:?>
                            <?php endif;?>


                        </ul>
                        <span style="margin-top: 3em;">Categorias</span>
                        <ul class="list">
                            <?php 
                            $args = array(
                                "number"     => "0",
                                "hide_empty" => "0"
                                );
                            $terms = get_terms('categoria-noticia',$args);
                            foreach ($terms as $term) {
                                $cat = $term->slug;
                                echo"<li><a href='". get_term_link( $term->term_id,'categoria-noticia') ."'>". $term->name ."</a></li>";
                            } 
                            ?>
                        </ul>

                        <span style="margin-top: 3em;">Tags</span>
                        <ul class="list">
                             <?php 
                            $args = array(
                                "number"     => "0",
                                "hide_empty" => "0"
                                );
                            $terms = get_terms('tag-noticias',$args);
                            foreach ($terms as $term) {
                                $cat = $term->slug;
                                echo"<li><a href='". get_term_link( $term->term_id,'tag-noticias') ."'>". $term->name ."</a></li>";
                            } 
                            ?>
                        </ul>
                    </aside>
                </div>
            </div>
        </section>
    </main>
    <?php endif;?>
<?php get_footer();?>