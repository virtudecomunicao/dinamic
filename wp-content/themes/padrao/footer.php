    <footer class="footer">
<div class="container">
    <div class="row">
    <div class="col s12 m12 l4 img-logo"></div>
    
    <div class="col s12 m12 l4 endereco">
        <h4>Venha conhecer nosso novo endereço!</h4>
        <p><?php echo wpautop(get_option('endereco'));?></p>
        <?php 
        $telefone = explode(' ', get_option('phone'));
        $celular = explode(' ', get_option('celular'));

        ?>
        <span><sm><?php echo $telefone[0];?></sm> <?php echo $telefone[1];?> | <sm><?php echo $celular[0];?></sm> <?php echo $celular[1];?></span>
    </div>
    
    <div class="col s12 m12 l4 contato">
        <h4>Entre em contato e siga-nos!</h4>
        <ul>
            <li><a href="mailto:<?php echo get_option('email');?>"><i class="fa fa-envelope-o"></i></a></li>
            <li><a href="<?php echo get_option('link_facebook');?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php echo get_option('link_linkedin');?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
    </div>
    </div>
    <div class="row copy">
        <div class="col s12 l6 copyright">
            <p><i class="fa fa-copyright"></i> COPYRIGHT 2016. - Todos os direitos reservados Vihabile.</p>
        </div>
        <div class="col s12 l6 virtude">
            <p class="virtude">Desenvolvido por <img src="<?php echo IMAGES; ?>/virtude.png" alt="Logo Agência Virtude Comunicação"></p>
        </div>
    </div>
    </div>
</div>''
    <?php if (is_page('contato')) : ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACzyWtTtqD5rGjsqem5WUOTWwS2FvxOKU&amp;sensor=false"></script> 
        
    <?php endif; ?>
    <?php wp_footer(); ?>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
</body>
</html>