<?php get_header(); ?>
<!-- ||||||||||||||||||||||||  Silder |||||||||||||||||||||||||||||||||||||-->
 <header>
    <div class="slider">
        <ul class="slides">
        <?php 
                $args = array(
                    'post_type'   => 'slider',
                    'post_status' => 'publish',
                    'order'       => 'desc',
                    );
                $query = new WP_Query( $args ); 
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
                 $slides = get_post_meta(get_the_id(),'thumb', false);
                foreach ($slides as $slide) { ?>
                <li><img src="<?php echo wp_get_attachment_url($slide); ?>"></li> <!-- random image -->
                <?php } endwhile; else: ?>
                <li><img src="<?php echo IMAGES; ?>/slider/slide1.png"></li>
                <li><img src="<?php echo IMAGES; ?>/slider/slide2.png"></li>
                <li><img src="<?php echo IMAGES; ?>/slider/slide3.png"></li>
                <li><img src="<?php echo IMAGES; ?>/slider/slide4.png"></li>
                <?php endif; wp_reset_postdata();?>
        </ul>
    </div>
</header>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->
<main>
<section id="inicio">

    <div class="container">
        <div class="row">
            
            <div class="col s12 l6 arquitetura">
                <?php $args = array(
                    'name'        => 'arquitetura',
                    'post_type'   => 'servico',
                    'post_status' => 'publish',
                    'posts_per_page' => 1
                    );
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                <h3><?php echo the_title()?></h3>
                <p><?php echo get_post_meta( get_the_id(), 'box_home', true )?></p>
                <a href="<?php echo site_url(); ?>/servicos/#arquitetura"><span>SAIBA MAIS</span></a>
            <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>

            <div class="col s12 l6 viabilidade">
                <?php $args = array(
                    'name'        => 'viabilidade',
                    'post_type'   => 'servico',
                    'post_status' => 'publish',
                    'posts_per_page' => 1
                    );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                <h3><?php echo the_title()?></h3>
                <p><?php echo get_post_meta( get_the_id(), 'box_home', true )?></p>
                <a href="<?php echo site_url(); ?>/servicos/#viabilidade"><span>SAIBA MAIS</span></a>
            <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<!-- ||||||||||||||||||||||||  CLIENTES  |||||||||||||||||||||||||||||||||||||-->
<section id="clientes">
    <div class="container">
        <div class="row">
            <div class="col s12 l12">
                <h2>CLIENTES</h2>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12 carrosel">
                <?php $args = array(
                    'post_type'   => 'cliente',
                    'post_status' => 'publish',
                    );
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                    <a href="<?php echo get_post_meta( get_the_id(), 'link', true )?>"><div class="logo" style="background-image: url(<?php echo wp_get_attachment_url(get_post_meta( get_the_id(), 'logo', true )); ?>)"><span>Ver Projeto</span></div></a>
                <?php endwhile; endif; wp_reset_postdata(); ?>        
            </div>
        </div>
    </div>
</section>
<!-- ||||||||||||||||||||||||  DEPOIMENTOS    |||||||||||||||||||||||||||||||||||||-->
<section id="depoimentos">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h2>DEPOIMENTOS</h2>
            </div>
            <?php $args = array(
                    'post_type'   => 'depoimentos',
                    'post_status' => 'publish',
                    'posts_per_page' => 3
                );
            $query = new WP_Query( $args );

            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
            if ( (get_post_meta( get_the_id(), 'tipo', true ) ) == "arquitetura" ) $box = 'laranja';
            if ( (get_post_meta( get_the_id(), 'tipo', true ) ) == "viabilidade" ) $box = 'verde';
            ?>
            <div class="col s12 m12 l4 box" >
                <div class="box-<?php echo $box;?>">
                    <div class="img" style="background-image: url(<?php echo wp_get_attachment_url(get_post_meta( get_the_id(), 'imagem_perfil', true )); ?>)"></div>
                    <h3><?php echo the_title()?></h3>
                    <p><?php echo get_post_meta( get_the_id(), 'descricao', true );?></p>
                </div>
            </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
    </div>
</section>
</main>

<?php get_footer(); ?>