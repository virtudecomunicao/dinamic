<!doctype html>
<html class="no-js" lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Website Title</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="480" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
    <link rel="stylesheet" href="<?php echo CSS; ?>/styles.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body id="home">
<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
    <header>
    <div class="slider">
        <ul class="slides">
            <li>
                <img src="<?php echo IMAGES; ?>/slider/slide1.png"> <!-- random image -->
            </li>
            <li>
                <img src="<?php echo IMAGES; ?>/slider/slide2.png"> <!-- random image -->
            </li>
            <li>
                <img src="<?php echo IMAGES; ?>/slider/slide3.png"> <!-- random image -->
            </li>
            <li>
                <img src="<?php echo IMAGES; ?>/slider/slide4.png"> <!-- random image -->
            </li>
        </ul>
    </div>
</header>
<!-- ||||||||||||||||||||||||  MENU |||||||||||||||||||||||||||||||||||||-->
<nav>
    <div id="menuButton"></div>
    <div class="container">
        <div class="row">
            <a href="index.html">
                <h3 class="logo">Vihabile - Arquitetura e Viabilidade</h3>
                <div class="logocolor">Vihabile - Arquitetura e Viabilidade</div>
            </a>
            <div class="menu">
                <ul>
                    <li><a href="index.html">HOME</a>|</li>
                    <li><a href="_quem.html">QUEM SOMOS</a>|</li>
                    <li><a href="_servicos.html">SERVIÇOS</a>|</li>
                    <li><a href="_portfolio.html">PORTIFÓLIO</a>|</li>
                    <li><a href="_noticias.html">NOTÍCIAS</a>|</li>
                    <li><a href="_clientes.html">CLIENTES</a>|</li>
                    <li><a href="_contato.html">CONTATO</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->

<main>
<section id="inicio">

    <div class="container">
        <div class="row">
            
            <div class="col s12 l6 arquitetura">
                <h3>Arquitetura</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat magna et erat pulvinar bibendum. Quisque ultricies quis neque ornare pellentesque. Curabitur condimentum leo et nisl bibendum ultricies in vel metus. Fusce placerat sollicitudin interdum. Nam feugiat tellus sed orci hendrerit, vitae aliquet dui ultricies.
                </p>
                <a href=""><span>SAIBA MAIS</span></a>
            </div>
            <div class="col s12 l6 viabilidade">
                <h3>Viabilidade</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat magna et erat pulvinar bibendum. Quisque ultricies quis neque ornare pellentesque. Curabitur condimentum leo et nisl bibendum ultricies in vel metus. Fusce placerat sollicitudin interdum. Nam feugiat tellus sed orci hendrerit, vitae aliquet dui ultricies.
                </p>
                <a href=""><span>SAIBA MAIS</span></a>

            </div>
        </div>
    </div>
</section>
<!-- ||||||||||||||||||||||||  CLIENTES  |||||||||||||||||||||||||||||||||||||-->
<section id="clientes">
    <div class="container">
        <div class="row">
            <div class="col s12 l12">
                <h2>CLIENTES</h2>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12 carrosel">                
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente1.png)"><span>Ver Projeto</span></div></a>
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente2.png)"><span>Ver Projeto</span></div></a>
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente3.png)"><span>Ver Projeto</span></div></a>           
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente4.png)"><span>Ver Projeto</span></div></a> 
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente2.png)"><span>Ver Projeto</span></div></a>
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente3.png)"><span>Ver Projeto</span></div></a>           
                    <a href=""><div class="logo" style="background-image: url(<?php echo IMAGES; ?>/clientes/Cliente4.png)"><span>Ver Projeto</span></div></a>         
            </div>
        </div>
    </div>
</section>
<!-- ||||||||||||||||||||||||  DEPOIMENTOS    |||||||||||||||||||||||||||||||||||||-->
<section id="depoimentos">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h2>DEPOIMENTOS</h2>
            </div>
            <div class="col s12 m12 l4 box" >
                <div class="box-laranja">
                <div class="img" style="background-image: url(<?php echo IMAGES; ?>/depoimentos/depoimentos.png)"></div>
                    <h3>RENATA MARTINS</h3>
                    <p>Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum nisi, mattis placerat arcu. Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                
            </div>
            <div class="col s12 m12 l4 box">
                <div class="box-verde">
                <div class="img" style="background-image: url(<?php echo IMAGES; ?>/depoimentos/depoimentos.png)"></div>

                    <h3>FRANCYELE FRANCISCA</h3>
                    <p>Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum nisi, mattis placerat arcu. Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col s12 m12 l4 box">
                <div class="box-laranja">
                <div class="img" style="background-image: url(<?php echo IMAGES; ?>/depoimentos/depoimentos.png)"></div>

                    <h3>GABRIELA MOURA</h3>
                    <p>Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum nisi, mattis placerat arcu. Sed accumsan nisl sed ante varius accumsan. Praesent sit amet fermentum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </div>
</section>



</main>
<footer class="footer">
<div class="container">
    <div class="row">
    <div class="col s12 m12 l4 img-logo"></div>
    
    <div class="col s12 m12 l4 endereco">
        <h4>Venha conhecer nosso novo endereço!</h4>
        <p> Av. José Rocha Bonfim, 214 | Jardim Santa Genebra<br>
            Campinas - SP | 13080-650<br>
            Praça Capital | Ed. Nova Iorque | Sala 520 </p>
        <span><sm>19</sm> 3201.7519 | <sm>19</sm> 8154.7661</span>
    </div>
    
    <div class="col s12 m12 l4 contato">
        <h4>Entre em contato e siga-nos!</h4>
        <ul>
            <li><a href=""><i class="fa fa-envelope-o"></i></a></li>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
        </ul>
        
    </div>
    </div>
    <div class="row copy">
        <div class="col s12 l6 copyright">
            <p><i class="fa fa-copyright"></i> COPYRIGHT 2016. - Todos os direitos reservados Vihabile.</p>
        </div>
        <div class="col s12 l6 virtude">
            <p class="virtude">Desenvolvido por <img src="<?php echo IMAGES; ?>/virtude.png" alt="Logo Agência Virtude Comunicação"></p>
        </div>
    </div>
    </div>
</div>''
    <!-- <div class="container">
        <p class="copyright">COPYRIGHT &copy; 2016 - Todos os Direitos Reservados Vihabile</p>
        <p class="virtude">Desenvolvido por <img src="<?php echo IMAGES; ?>/virtude.png" alt="Logo Agência Virtude Comunicação"></p>
    </div> -->
</footer>

<!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD89OabSMiyYTSE91otbjynWWireeXwGIQ&amp;sensor=false"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
<script src="scripts/app.js"></script>
</body>
</html>
