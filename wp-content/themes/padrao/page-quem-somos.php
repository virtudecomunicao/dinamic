<?php
/**
 * The template page: Contato
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
get_header(); ?>
<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
    <header style="background-image: url(<?php echo IMAGES; ?>/bg-quem.jpg);">
    
    </header>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->

<main>
<section id="quem">
    <div class="title">
        <h2>Quem Somos</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col l6 m12 s12 textos">
            <?php $args = array(
                    'post_type'   => 'sobre',
                    'post_status' => 'publish',
                    );
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                    <?php echo wpautop(get_post_meta( get_the_id(), 'descricao', true ));?>
                <?php endwhile; endif; wp_reset_postdata(); ?> 
            </div>
            <div class="col l6 m12 s12 mocas">
                <img src="<?php echo IMAGES; ?>/licia-e-iara.png" alt="Lícia e Iara">
                <span class="licia">Lícia Ribeiro</span><span class="iara">Iara Plepis</span>
            </div>
        </div>
    </div>
</section>

</main>
<?php get_footer(); ?>