<!doctype html>
<html class="no-js" lang="pt-br">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="480" />

    <?php wp_head(); ?>
</head>
<?php if (is_home()) : ?>
<body id="home">
<?php else : ?>
<body id="page">
<?php endif; ?>

<!-- ||||||||||||||||||||||||  MENU aqui |||||||||||||||||||||||||||||||||||||-->
<nav>
    <div id="menuButton"></div>
    <div class="container">
        <div class="row">
            <a href="index.html">
                <h3 class="logo">Vihabile - Arquitetura e Viabilidade</h3>
                <div class="logocolor">Vihabile - Arquitetura e Viabilidade</div>
            </a>
            <div class="menu">
                <ul>
                    <li><a href="<?php echo site_url(); ?>">HOME</a></li>
                <?php
                    $links = array (
                        'quem-somos' => 'QUEM SOMOS',
                        'servicos'   => 'SERVIÇOS',
                        'portfolios'   => 'PORTFÓLIO',
                        'noticias'    => 'NOTÍCIAS',
                        'clientes'    => 'CLIENTES',
                        'contato'    => 'CONTATO',
                        );
                    foreach($links as $key => $value) {
                        echo '<li><a href="' . get_permalink(get_page_by_path($key)) .'">' . $value . '</a></li>';
                    };
                ?>
                </ul>
            </div>
        </div>
    </div>
</nav>
