(function(){
    var loadPosts = $('.loadmore');

    loadPosts.on('click',function(){
        var url = $(this).attr('href');
        $(this).html('CARREGANDO');

        $.get(url,function(data){
            $('#loadportfolio').append($(data).find('#loadportfolio'));

            // get new next link from data
            var nextLink = $(data).find('.loadmore').attr('href');

            // verify if link exists
            if (undefined == nextLink) {
                loadPosts.hide();
            }
            else {
                loadPosts.html('Carregar Mais');
                loadPosts.attr('href',nextLink);
            }
        });

        return false;
    });
})(jQuery);


/**
* Load More Cliente com ajax
**/

(function(){
    var loadPosts = $('.loadmore');

    loadPosts.on('click',function(){
        var url = $(this).attr('href');
        $(this).html('CARREGANDO');

        $.get(url,function(data){
            $('#loadclientes').append($(data).find('#loadclientes'));

            // get new next link from data
            var nextLink = $(data).find('.loadmore').attr('href');

            // verify if link exists
            if (undefined == nextLink) {
                loadPosts.hide();
            }
            else {
                loadPosts.html('Carregar Mais');
                loadPosts.attr('href',nextLink);
            }
        });

        return false;
    });
})(jQuery);