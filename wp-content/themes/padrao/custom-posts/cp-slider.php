<?php
/**
 * Custom Post Type: Slider
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

defined('ABSPATH') OR exit('No direct script access allowed');

$labels = array(
	'name'               => _x('Slider', 'post type general name'),
	'singular_name'      => _x('Slider', 'post type singular name'),
	'add_new'            => _x('Adicionar Novo slider', 'Novo slider'),
	'add_new_item'       => __('Novo slider'),
	'edit_item'          => __('Editar slider'),
	'new_item'           => __('Novo slider'),
	'view_item'          => __('Ver sliders'),
	'search_items'       => __('Procurar slider'),
	'not_found'          => __('Nenhum slider encontrado'),
	'not_found_in_trash' => __('Nenhum slider encontrado na lixeira'),
	'parent_item_colon'  => '',
	'menu_name'          => 'Slider'
	);

$args = array(
	'labels'             => $labels,
	'public'             => true,
	'supports'           => array('title'),
		'exlude_from_search' => 'true' // Whether to exclude posts with this post type from front end search results.
		);

register_post_type('slider', $args);
flush_rewrite_rules();

