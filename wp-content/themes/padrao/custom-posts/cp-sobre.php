<?php
/**
 * Custom Post Type: Sobre
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

defined('ABSPATH') OR exit('No direct script access allowed');

$labels = array(
	'name'               => _x('Sobre', 'post type general name'),
	'singular_name'      => _x('Sobre', 'post type singular name'),
	'add_new'            => _x('Adicionar Novo Sobre', 'Novo Sobre'),
	'add_new_item'       => __('Novo Sobre'),
	'edit_item'          => __('Editar Sobre'),
	'new_item'           => __('Novo Sobre'),
	'view_item'          => __('Ver Sobres'),
	'search_items'       => __('Procurar Sobre'),
	'not_found'          => __('Nenhum Sobre encontrado'),
	'not_found_in_trash' => __('Nenhum Sobre encontrado na lixeira'),
	'parent_item_colon'  => '',
	'menu_name'          => 'Quem Somos'
	);

$args = array(
	'labels'             => $labels,
	'public'             => true,
	'supports'           => array('title'),
		'exlude_from_search' => 'true' // Whether to exclude posts with this post type from front end search results.
		);

register_post_type('sobre', $args);
flush_rewrite_rules();

