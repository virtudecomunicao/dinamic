<?php 
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

get_header();?>
<?php if ( have_posts() ) : the_post(); $titulo = the_title(); ?>

<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
    <header style="background-image: url(<?php echo IMAGES; ?>/port-placeholder.jpg);">
        
    </header>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->

<main>
<section id="port">
    <div class="title">
        <h2><?php echo get_the_title();?> <span class="category">(<?php $terms = get_the_terms(get_the_id(),'categoria-portfolio');
                                                    foreach ($terms as $term) { echo $term->name;}?>)</span></h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col l12 m12 s12 desc">
                <?php echo wpautop(get_post_meta( get_the_id(), 'descricao_portfolio', true ));?>
            </div>
            <div class="col l12 m12 s12">
                <div id="carrousel-big">
                    <?php $pages = get_post_meta(get_the_id(),'galeria_portfolio', false);
                    foreach ($pages as $key=>$single) {?>

                        <div class="slide" style="background-image: url(<?php echo wp_get_attachment_url( $single['imagem_portfolio'] ); ?>)">
                            <div class="caption">
                            <?php echo $single['desc_img'];?>
                            </div>
                        </div>

                        <?php  }  ?>
                    
                </div>
                <div id="carrousel-small">
                <?php $pages = get_post_meta(get_the_id(),'galeria_portfolio', false);
                    foreach ($pages as $key=>$single) {?>
                    <div class="slide" style="background-image: url(<?php echo wp_get_attachment_url( $single['imagem_portfolio'] ); ?>)"></div>
                        <?php }  ?>
              </div>
            </div>
        </div>
    </div>
</section>

</main>

<?php endif;?>
<?php get_footer();?>