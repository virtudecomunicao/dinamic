<?php
/**
 * The template page: Clientes
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
get_header(); ?>
<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
<header style="background-image: url(<?php echo IMAGES; ?>/bg-clientes.jpg);"></header>
<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->
<main>
    <section id="clientes">
        <div class="title">
            <h2>Clientes</h2>
        </div>
        <div class="container">
            <div id="loadclientes" class="row">
                <?php
                // set the "paged" parameter (use 'page' if the query is on a static front page)
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

                // the query
                $the_query = new WP_Query( 'post_type=cliente&posts_per_page=12&paged=' . $paged . '&order=asc' ); 
                ?>

                <?php if ( $the_query->have_posts() ) : ?>

                    <?php
                    // the loop 
                    while ( $the_query->have_posts() ) : $the_query->the_post(); 
                    ?>

                    <!-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->

                    <div class="col l3 m3 s6">
                        <a style="background-image: url(<?php echo wp_get_attachment_url( get_post_meta( get_the_id(), 'logo', true )); ?>);" class="cliente" href="<?php echo get_post_meta( get_the_id(), 'link', true )?>" rel="nofollow"></a>
                    </div>

                    <!-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->

                <?php endwhile; ?>
            </div>
            <div class="row" style="text-align: center;">
                <?php echo str_replace('<a href','<a class="loadmore" style="display: inline-block;"  href', get_next_posts_link( 'Carregar mais', $the_query->max_num_pages )); ?>
            </div>
        <?php else:  ?>
            <p><?php _e( 'Desculpa, não foi encontrado posts nesse critério' ); ?></p>
        <?php endif; wp_reset_postdata(); ?> 
</div>
</section>
</main>
<?php get_footer(); ?>