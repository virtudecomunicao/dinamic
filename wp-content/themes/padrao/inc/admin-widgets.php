<?php
/**
 * Actions and Filter for functions.php
 * 
 * Custom widgets for admin panel
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

defined('ABSPATH') OR exit('No direct script access allowed');

/**
 * Set Dashboard Widgets
 * */
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');
function custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('contact_info', 'Informações de Contato', 'contact_info_widget_template');
	wp_add_dashboard_widget('social_info', 'Informações de Redes Sociais', 'social_info_widget_template');
	wp_add_dashboard_widget('help_info', 'Ajuda', 'help_widget_template');

}
/**
 * Create Options if not exists
 * 
 * */
add_action('admin_init','verify_options');
function verify_options() {
	// Informações de Contato
	if ( false == get_option('phone') ) add_option('phone','');
	if ( false == get_option('celular') ) add_option('celular','');
	if ( false == get_option('endereco') ) add_option('endereco','');
	if ( false == get_option('email') ) add_option('email','');
	if ( false == get_option('facebook') ) add_option('facebook','');
	if ( false == get_option('linkedin') ) add_option('linkedin','');
	if ( false == get_option('link_facebook') ) add_option('link_facebook','');
	if ( false == get_option('link_linkedin') ) add_option('link_linkedin','');

}
/**
 * Template for Contact Widget
 * */
function contact_info_widget_template() {
	$contact_info_update = false;

	if (isset($_POST['save_info_contact'])) {

		$data = filter_input_array(INPUT_POST,
			array(
				"phone"    => FILTER_SANITIZE_STRING,
				"celular"    => FILTER_SANITIZE_STRING,
				"endereco" => FILTER_SANITIZE_STRING,
				)
			);
		update_option('phone', $data['phone']);
		update_option('celular', $data['celular']);
		update_option('endereco', $data['endereco']);
		


		$contact_info_update = true;
	}

	if ($contact_info_update)
		echo '<div class="updated"><p>Informações de Contato Atualizadas com sucesso.</p></div>';

	// Widget Template
	require get_theme_root() . "/vihabile/widgets/wd-contato.php";
}

/**
 * Template for Social Widget
 * */
function social_info_widget_template() {
	$social_info_update = false;

	if (isset($_POST['save_social_info'])) {

		$data = filter_input_array(INPUT_POST,
			array(
				"email"   => FILTER_SANITIZE_EMAIL,
				"facebook"   => FILTER_SANITIZE_URL,
				"linkedin"   => FILTER_SANITIZE_URL,
				"link_facebook"   => FILTER_SANITIZE_URL,
				"link_linkedin"   => FILTER_SANITIZE_URL,
				)
			);
		update_option('email', $data['email']);
		update_option('facebook', $data['facebook']);
		update_option('linkedin', $data['linkedin']);
		update_option('link_facebook', $data['link_facebook']);
		update_option('link_linkedin', $data['link_linkedin']);


		$social_info_update = true;
	}

	if ($social_info_update)
		echo '<div class="updated"><p>Informações de Contato Atualizadas com sucesso.</p></div>';

	// Widget Template
	require get_theme_root() . "/vihabile/widgets/wd-social.php";
}


function help_widget_template() {

	// Widget Template
	require get_theme_root() . "/vihabile/widgets/wd-ajuda.php";
}