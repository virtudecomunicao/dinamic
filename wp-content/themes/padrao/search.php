<?php
/**
 * The template page: Noticias
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */


get_header(); ?>

<header id="header" style="background-image: url(<?php echo IMAGES; ?>/header01.jpg);">
    <div class="title">
        <h2>Notícias</h2>
        <?php $s= $_GET['s'];?>
        <div class="bread">Você está aqui: Home / Notícias Pesquisa: <?php echo $s;?></div>

    </div>
</header>

<main>
        <section id="noticias">
            <div class="column">
                <div class="row">
                    <div class="col l9">
                        <?php if ( have_posts() && strlen( trim(get_search_query()) ) != 0 ) : while ( have_posts() ) : the_post(); ?>
                        <article>
                            <header>
                                <div class="img" style="background-image: url(<?php echo wp_get_attachment_url( get_post_meta( get_the_id(), 'imagem_destacada', true )); ?>)"></div>
                                <h1><?php echo the_title();?></h1>
                                <time class="mini"><span class="day"><?php the_time('d');?></span><span class="month"><?php the_time('F');?></span></time>
                            </header>
                            <?php echo excerpt(65, 'descricao_noticia');?>

                            <div class="buttons">
                                <a href="<?php the_permalink(); ?>" class="read-more">Leia Mais</a>
                            </div>
                        </article>
                        <?php endwhile; else : ?> 
						<article><header>
						<h1>Não foi encontrado nenhuma notícia com o nome: <?php echo $s; ?>.</h1>
						<h1>Por favor tente outro nome.</h1>
						</header></article>
                    <?php endif; wp_reset_postdata(); ?>
                    </div>
                    <div class="col l3">
                        <div class="search">
                            <b>Busca</b>
                            <form class="form-search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <input type="text" name="s" value="" placeholder="Palavra-chave"/>
                                <input type="hidden" name="post_type" value="noticias" />
                                <button  class="pesquisa" type="submit" value="Buscar" id="searchsubmit"><i class="fa fa-search"></i></button>
                            </form>
                            
                        </div>
                        <div class="destaque">
                            <b>Populares</b>
                            <?php 
                            $args = array(
                                'post_type'      => 'noticias',
                                'posts_per_page' => 8,                 
                                'post_status'    => 'publish',         
                                'orderby'        => 'meta_value_num',  
                                'meta_key'       => 'tp_post_counter', 
                                'order'          => 'DESC'             
                                );
                            $query = new WP_Query( $args );

                             if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                            <a href="<?php the_permalink(); ?>"><?php echo the_title();?></a>
                            
                            <?php endwhile; endif; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>