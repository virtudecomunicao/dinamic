<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
get_header(); ?>
<header id="header" style="background-image: url(<?php echo IMAGES; ?>/header01.jpg);">
	<div class="title">
		<h2>Página Não Encontrada</h2>
		<div class="bread">Está página pode ter sido removida pelo autor. <a href="<?php echo site_url(); ?>">Voltar para Home</a></div>
	</div>
</header>

<?php get_footer(); ?>
