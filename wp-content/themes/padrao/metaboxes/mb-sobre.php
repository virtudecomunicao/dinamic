<?php
/**
 * Custom Metabox: Depoimentos
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed'); 

$fields = array(
	
	array (
		'id'        => 'descricao', 
		'name'      => 'Descrição da Página Quem Somos', 
		'type'      => 'wysiwyg',
		),
	);

$meta_boxes[] = array(
	'title'   => 'Quem Somos',
	'pages'   => 'sobre',
	'fields'  => $fields,
	);