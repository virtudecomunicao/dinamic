<?php
/**
 * Custom Metabox: Portfólio
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed'); 

$fields = array(
	array(
		'id'    => 'descricao_portfolio',
		'name' => 'Descrição do Portófolio',
		'type'  => 'textarea',
		),
	);

$meta_boxes[] = array(
	'title'   => 'Descrição do Portfólio',
	'pages'   => 'portfolio',
	'fields'  => $fields,
	);
$group = array (
	array (
		'id'        => 'imagem_portfolio', 
		'name'      => 'Imagem Galeria', 
		'type'      => 'image',
		'cols' => 2,
		),
		array (
		'id'   => 'desc_img',
		'name' => 'Descrição da Imagem',
		'type' => 'textarea',
		'cols' => 10,
		'rows' => 7
	),
	
);

$fields = array (
	array (
		'id'                  => 'galeria_portfolio',
		'name'                => 'Galeria de Fotos do Portfólio',
		'desc'                => 'Adição e remoção de imagens',
		'type'                => 'group',
		'fields'              => $group,
		'repeatable'          => true,
		'sortable'            => true,
		'string-repeat-field' => 'Adicionar imagem na Galeria',
		'string-delete-field' => 'Remover imagem da Galeria',
	),
);
$meta_boxes[] = array(
	'title'   => 'Galeria Portfólio',
	'pages'   => 'portfolio',
	'fields'  => $fields,
	);