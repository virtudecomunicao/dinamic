<?php
/**
 * Custom Metabox: Slider
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed'); 

$fields = array(
	
	array (
		'id'        => 'thumb', 
		'name'      => 'Icone', 
		'desc'      => 'Insira imagens nas dimensões largura 1366px e altura 768px.',
		'type'      => 'image',
		'repeatable' => true,
		'sortable' => true,
		'string-repeat-field' => 'Novo Slider',
		'string-delete-field' => 'Remover Imagem',

		),
	);

$meta_boxes[] = array(
	'title'   => 'Informação Imagem Destacada',
	'pages'   => 'slider',
	'fields'  => $fields,
	);


