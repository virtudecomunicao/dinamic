<?php
/**
 * Custom Metabox: Serviços
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed'); 

$fields = array(
	array(
		'id'    => 'box_home',
		'name' => 'Box Home',
		'type'  => 'textarea',
		),
	);

$meta_boxes[] = array(
	'title'   => 'Caixa da Home',
	'pages'   => 'servico',
	'fields'  => $fields,
	);
$group = array (
		array (
		'id'   => 'titulo_servico',
		'name' => 'Titulo Serviço',
		'type' => 'text',
	),
	array (
		'id'   => 'descricao_servico',
		'name' => 'Descricao do Serviço',
		'type' => 'textarea',
		'rows' => 10
	),
	
);

$fields = array (
	array (
		'id'                  => 'tipos_servico',
		'name'                => 'Tipo de Serviço',
		'desc'                => 'Adição e remoção de serviços',
		'type'                => 'group',
		'fields'              => $group,
		'repeatable'          => true,
		'sortable'            => true,
		'string-repeat-field' => 'Adicionar tipo de servico',
		'string-delete-field' => 'Remover tipo de serviço',
	),
);
$meta_boxes[] = array(
	'title'   => 'Serviços',
	'pages'   => 'servico',
	'fields'  => $fields,
	);