<?php


/*
*	custom admin menu
*	ver 1.0
*/ 
function virtude_new_menu () {
	add_action('admin_enqueue_scripts','wp_load_menu');
	function wp_load_menu() {
		wp_enqueue_style('WP_MENU_CSS',get_stylesheet_directory_uri() . '/virtude/new_menu/wp_menu.css',array(),'1.0');
		wp_enqueue_script('WP_MENU_JS',get_stylesheet_directory_uri() . '/virtude/new_menu/wp_menu.min.js',array(),'1.0',true);
		wp_enqueue_script('WP_MY_MENU',get_stylesheet_directory_uri() . '/virtude/new_menu/my_menu.js',array(),'1.0',true);
		wp_enqueue_script('WP_MENU_START',get_stylesheet_directory_uri() . '/virtude/new_menu/wp_menu_start.js',array(),'1.0',true);
	}
}

/*
*	virtude copyright footer
*	ver 1.0
*/ 
function virtude_copyright () {
	add_filter('admin_footer_text', 'add_copyright');
	function add_copyright() {
		echo '© <a href="http://agenciavirtude.com.br/">Virtude</a> - Desenvolvimento Virtude Comunicação';
	}

}
/*
*	custom login screen
*	ver 1.0
*/ 

function virtude_login ($img) {
	add_action( 'login_enqueue_scripts', function () use ($img) {
		?>
		<style>
		body {
	        background-color: #000 !important;
	        background-image: url('<?php echo get_stylesheet_directory_uri() ?>/images/<?php echo $img ?>') !important;
	        background-size: cover !important;
	        background-repeat: no-repeat !important;
	        background-position: center !important;
	        color: #fff !important;
	    }

	    a {
	        color: #fff !important; 
	    }

	    .login .message, .login #login_error {
	        padding: 12px !important;
	        margin-left: 0 !important;
	        background-color: transparent !important;
	        -webkit-box-shadow: none !important;
	        box-shadow: none !important;
	        color: #fff !important;
	    }
	    .login label {
	        color: #fff !important; 
	    }
	    /* labels name and password */
	    #login {
	        width: 320px !important;
	        background-color: rgba(0,0,0,.7) !important;
	        position: absolute !important;
	        top: 0 !important;
	        right: 0 !important;
	        left: 0 !important;
	        margin: auto !important;
	        padding: 0 !important;
	        border-radius: 8px !important;
	    }
	    /* Site Logo */
	    .login h1 a {
	        background-image: url('<?php echo get_stylesheet_directory_uri() ?>/images/logo.png') !important;
	        background-size: contain !important;
	        background-position: center !important;
	        background-repeat: no-repeat !important;
	        width: 100% !important;
	        height: 80px !important;
	        margin: 8px 0 0 !important;
	    }
	    /* Submit button */
	    .login form {
	        margin-top: 0 !important;
	        margin-left: 0 !important;
	        padding: 18px !important;
	        background: transparent !important;
	        -webkit-box-shadow: none !important;
	        box-shadow: none !important;
	    }

	</style>
	
		<?php
	}, 10, 1 );
	// Set logo a href to the site URL

	function change_wp_login_url() {
		return site_url();
	}
	function change_wp_login_title() {
		//return bloginfo();
	}

	add_filter('login_headerurl', 'change_wp_login_url');
	add_filter('login_headertitle', 'change_wp_login_title');
	wp_enqueue_script('WP_LOGIN_JS',get_stylesheet_directory_uri() . '/virtude/login-screen/login.min.js',array(),'1.0',true);
}


