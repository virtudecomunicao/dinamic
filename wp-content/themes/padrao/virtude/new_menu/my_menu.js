menu.add({name: 'Painel - Contato', icon: 'tachometer', url: 'index.php'});

menu.add({name: 'divider'});

menu.add({name: 'Slider', icon: 'picture-o', url: 'post.php?post=114&action=edit'});

menu.add({name: 'divider'});

menu.add({name: 'Página Quem Somos', icon: 'users', url: 'post.php?post=144&action=edit'});

menu.add({name: 'divider'});

menu.add({name: 'Página Serviços', icon: 'suitcase'});
menu.add({name: 'Arquitetura',url: 'post.php?post=122&action=edit', sub: true});
menu.add({name: 'Viabilidade',url: 'post.php?post=121&action=edit', sub: true});

menu.add({name: 'divider'});

menu.add({name: 'Administrar Portfólios', icon: 'suitcase'});
menu.add({name: 'Ver Todos',url: 'edit.php?post_type=portfolio', sub: true});
menu.add({name: 'Adicionar Novo',url: 'post-new.php?post_type=portfolio', sub: true});
menu.add({name: 'Categorias',url: 'edit-tags.php?taxonomy=categoria-portfolio&post_type=portfolio', sub: true});

menu.add({name: 'divider'});

menu.add({name: 'Administrar Notícias', icon: 'bullhorn'});
menu.add({name: 'Ver Todas', url: 'edit.php?post_type=noticias', sub: true});
menu.add({name: 'Adicionar Nova', url: 'post-new.php?post_type=noticias', sub: true});
menu.add({name: 'divider', sub: true});
menu.add({name: 'Categorias', url: 'edit-tags.php?taxonomy=categoria-noticia&post_type=noticias', sub: true});
menu.add({name: 'Tags', url: 'edit-tags.php?taxonomy=tag-noticias&post_type=noticias', sub: true});

menu.add({name: 'divider'});

menu.add({name: 'Gestão de Clientes', icon: 'suitcase'});
menu.add({name: 'Ver Todos',url: 'edit.php?post_type=cliente', sub: true});
menu.add({name: 'Adicionar Novo',url: 'post-new.php?post_type=cliente', sub: true});

menu.add({name: 'divider'});

menu.add({name: 'Administrar Depoimentos', icon: 'quote-left', url: 'edit.php?post_type=depoimentos'});
menu.add({name: 'Ver Todos', url: 'edit.php?post_type=depoimentos', sub: true});
menu.add({name: 'Adicionar', url: 'post-new.php?post_type=depoimentos', sub: true});	

menu.add({name: 'divider'});