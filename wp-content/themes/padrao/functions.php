<?php
/**
 * Virtude Padrão functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Virtude Padrão
 */

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/**
 * Remove wordpress version | wlwmanifest | RSD
 * */
remove_action( 'wp_head', 'wp_generator' );
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

/**
 * Remove admin bar when logged
 * */
add_filter('show_admin_bar', '__return_false');

/*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
 */
add_theme_support( 'post-thumbnails' );
add_image_size('portfolio',352,283);

/**
 * Constants Definition 
 * 
 * */
define('THEMEROOT', get_stylesheet_directory_uri());
define('FONTS', THEMEROOT . '/fonts');
define('IMAGES', THEMEROOT . '/images');
define('CSS', THEMEROOT . '/styles');
define('JS', THEMEROOT . '/scripts');
define('HOME', site_url() . "/");
/**
 * Add meta boxes to the administrative interface using HM Custom Meta Boxes for WordPress
 * For more information about metaboxes
 * @link https://codex.wordpress.org/Function_Reference/add_meta_box
 * 
 * For more information about HM Custom Meta Boxes for WordPress
 * @link https://github.com/humanmade/Custom-Meta-Boxes
 * Wiki
 * @link https://github.com/humanmade/Custom-Meta-Boxes/wiki 
 * 
 * */
// load class inside mu-plugins { Must-Use Plugins}
require WPMU_PLUGIN_DIR.'/Custom-Meta-Boxes/custom-meta-boxes.php';

add_filter( 'cmb_meta_boxes', 'register_metaboxes' );
function register_metaboxes( array $meta_boxes ) {
	
	$mbs = glob(get_theme_root() . '/padrao/metaboxes/mb-*.php');
	foreach ($mbs as $mb) {
		require $mb;
	}	
	return $meta_boxes;
}

 if ( ! class_exists('CMB_Meta_Box') ) {
 	add_action( 'admin_notices', 'myadmin_notices' );
 	return;
 }

 function myadmin_notices () {
 		echo '<div class="error"><p>Para um bom funcionamento do site e do painel administrativo, verifique se o HM Custom Meta Boxes for WordPress está instalado.</p></div>';
 }

/**
 * Create or modify a post type
 * 
 * form more information
 * @link https://codex.wordpress.org/Function_Reference/register_post_type 
 * 
 **/
add_action('init','register_post_types');
function register_post_types() {
	$cps = glob(get_theme_root() . '/padrao/custom-posts/cp-*.php');
	foreach ($cps as $cp) {
		require $cp;
	}
}

/**
 * Create or modify a taxonomy
 * 
 * form more information
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 * 
 **/
add_action('init','register_taxonomies');
function register_taxonomies() {
  $taxs = glob(get_theme_root() . '/padrao/taxonomies/tax-*.php');
  foreach ($taxs as $tax) {
    require $tax;
  }
}

add_action('wp_enqueue_scripts','padrao_scripts');

function padrao_scripts() {
	wp_deregister_script('jquery');
  
  wp_enqueue_style('materialize-css','https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css',array(),'0.97.5');
  wp_enqueue_style('slick-css','https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css',array(),'1.5.9');
  wp_enqueue_style('padrao-stylesheet', get_stylesheet_uri(), array(),'1.0');
	wp_enqueue_style('font-awesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css',array(),'4.6.1');

	wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js',array(),'2.1.4',true);
  wp_enqueue_script('materialize-js','https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js',array(),'0.97.5',true);
	wp_enqueue_script('slick-js','https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js',array(),'1.5.9',true);
	wp_enqueue_script('padrao-app', JS . '/app.js',array( 'jquery' ),'1.1',true);
  wp_enqueue_script( 'my-ajax-request',  JS . '/ajax.js', array(), '2.0', true );
  wp_localize_script( 'my-ajax-request', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
  // if (is_page('contato')){
  // wp_enqueue_script('padrao-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyACzyWtTtqD5rGjsqem5WUOTWwS2FvxOKU&amp;sensor=false',array(),'1.1',true);
  // }
}

add_action('admin_enqueue_scripts','padrao_admin_scripts');
function padrao_admin_scripts() {
  wp_enqueue_style('font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',array(),'4.5.0');
  // wp_enqueue_style('admin-custom-stylesheet',CSS . '/admin.css',array(),'1.0');
  wp_enqueue_script('allow-user-login',JS . '/allow-user-login.js',array(),'1.0',true);
  wp_enqueue_script('admin-scripts',JS . '/admin.js',array(),'1.0',true);

}




require 'inc/admin-widgets.php';

/**
 * Ajax functions
 * @see inc/ajax.php
 * */
require 'inc/ajax.php';



/**
 * Função que limita a quantidade de palavras exibidas no post
 * basta passar quantas palavras quer exibir e o tipo de post
 **/
function limit($limit, $type) {
  $excerpt = explode(' ', $type, $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

function excerpt($limit, $value) {
  $excerpt = explode(' ', $value, $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

function excerptNoAutop($limit, $type) {
  $excerpt = explode(' ',get_post_meta(get_the_id(), $type, true), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
/**
 * Função para criar listagem de post mais vistos
 **/
// // Verifica se não existe nenhuma função com o nome tp_count_post_views
// if ( ! function_exists( 'tp_count_post_views' ) ) {
//     // Conta os views do post
//     function tp_count_post_views () { 
//         // Garante que vamos tratar apenas de posts
//         if ( is_single() ) {
        
//             // Precisamos da variável $post global para obter o ID do post
//             global $post;
            
//             // Se a sessão daquele posts não estiver vazia
//             if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {
                
//                 // Cria a sessão do posts
//                 $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;
            
//                 // Cria ou obtém o valor da chave para contarmos
//                 $key = 'tp_post_counter';
//                 $key_value = get_post_meta( $post->ID, $key, true );
                
//                 // Se a chave estiver vazia, valor será 1
//                 if ( empty( $key_value ) ) { // Verifica o valor
//                     $key_value = 1;
//                     update_post_meta( $post->ID, $key, $key_value );
//                 } else {
//                     // Caso contrário, o valor atual + 1
//                     $key_value += 1;
//                     update_post_meta( $post->ID, $key, $key_value );
//                 } // Verifica o valor
                
//             } // Checa a sessão
            
//         } // is_single
        
//         return;
        
//     }
//     add_action( 'get_header', 'tp_count_post_views' );
// }


/**
 * Função que retona o a variavel, sem acentos, sem espaço e tudo em minusculo
 * Pega a variavel e transforma em um slug para consultar o post
 **/
function consult($string){
  $variavel = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
  $variavel = str_replace(" ", "-", $variavel);
  $variavel = strtolower($variavel);
    return $variavel;
}


/**
 * Função para pegar o slug do post
 **/


// function get_the_slug() {

// global $post;

// if ( is_single() || is_page() ) {
// return $post->post_name;
// }
// else {
// return "";
// }

// }


/** 
----------------------------------------------------------------------------------------------------------
 * Função para fazer pesquisa infinita, sem redirecionar para a home
 * @see http://www.evagoras.com/2012/11/01/how-to-handle-and-customize-an-empty-search-query-in-wordpress/
----------------------------------------------------------------------------------------------------------
 * */
function SearchFilter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
      $query->is_search = true;
      $query->is_home = false;
   }
   return $query;
}
add_filter('pre_get_posts','SearchFilter');


/**
 * Hide some options on administrative interface 
 * */
add_action('wp_before_admin_bar_render', 'hide_adminbar_options');
function hide_adminbar_options() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('wp-logo');
  $wp_admin_bar->remove_menu('about');
  $wp_admin_bar->remove_menu('wporg');
  $wp_admin_bar->remove_menu('documentation');
  $wp_admin_bar->remove_menu('support-forums');
  $wp_admin_bar->remove_menu('feedback');
  $wp_admin_bar->remove_menu('updates');
  $wp_admin_bar->remove_menu('comments');
  $wp_admin_bar->remove_menu('new-content');
  $wp_admin_bar->remove_menu('view');
  $wp_admin_bar->remove_menu('wpseo-menu'); // plugin yoast
}

require 'virtude/functions.php';
virtude_login('slide02.jpg');
virtude_copyright();
// virtude_new_menu();
?>