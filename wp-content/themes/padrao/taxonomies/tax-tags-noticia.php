<?php
/**
 * Custom Taxonomy: Tags Notícia
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

defined('ABSPATH') OR exit('No direct script access allowed');

$labels = array(
	'name'                       => _x( 'Tags', 'taxonomy general name' ),
	'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
	'search_items'               => __( 'Procurar tags' ),
	'all_items'                  => __( 'Todas as tags' ),
	'parent_item'                => null,
	'parent_item_colon'          => null,
	'edit_item'                  => __( 'Editar tag' ),
	'update_item'                => __( 'Atualizar tag' ),
	'add_new_item'               => __( 'Adicionar nova tag' ),
	'new_item_name'              => __( 'Nome da tag' ),
	'separate_items_with_commas' => __( 'Separe as tags utilizando vírgulas' ),
	'add_or_remove_items'        => __( 'Adicionar ou remover tags' ),
	'choose_from_most_used'      => __( 'Escolha as tags mais usadas' ),
	'not_found'                  => __( 'Nenhuma tag encontrada' ),
	'menu_name'                  => __( 'Tags Blog' ),
	);

$args = array(
	'hierarchical'      => false,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	);

register_taxonomy('tag-noticias','noticias',$args);