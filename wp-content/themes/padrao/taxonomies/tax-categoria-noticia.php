<?php
/**
 * Custom Taxonomy: Categoria Notícia
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */

defined('ABSPATH') OR exit('No direct script access allowed');

$labels = array(
	'name'                       => _x( 'Categorias', 'taxonomy general name' ),
	'singular_name'              => _x( 'Categoria', 'taxonomy singular name' ),
	'search_items'               => __( 'Procurar categoria' ),
	'all_items'                  => __( 'Todas as categorias' ),
	'parent_item'                => null,
	'parent_item_colon'          => null,
	'edit_item'                  => __( 'Editar categoria' ),
	'update_item'                => __( 'Atualizar categoria' ),
	'add_new_item'               => __( 'Adicionar nova categoria' ),
	'new_item_name'              => __( 'Nome da categoria' ),
	'separate_items_with_commas' => __( 'Separate writers with commas' ),
	'add_or_remove_items'        => __( 'Adicionar ou remover categoria' ),
	'choose_from_most_used'      => __( 'Escolha as categorias mais usados' ),
	'not_found'                  => __( 'Nenhuma categoria encontrada' ),
	'menu_name'                  => __( 'Categorias' ),
	);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	);

register_taxonomy('categoria-noticia','noticias',$args);