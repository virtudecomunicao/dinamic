<?php
/**
 * Dashboard Widget: Links Redes Sociais
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed');
?>

<form action="" method="post">
	<div class="cmb_metabox">

		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Email</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="email" value="<?php echo(get_option('email') == false) ? "" : get_option('email'); ?>"/>
			</div>
		</div>
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Facebook</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="facebook" value="<?php echo(get_option('facebook') == false) ? "" : get_option('facebook'); ?>" />
			</div>
		</div>
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Link: Facebook</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="link_facebook" value="<?php echo(get_option('link_facebook') == false) ? "" : get_option('link_facebook'); ?>" />
			</div>
		</div>

		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Linkedin</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="linkedin" value="<?php echo(get_option('linkedin') == false) ? "" : get_option('linkedin'); ?>" />
			</div>
		</div>
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Link: Linkedin</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="link_linkedin" value="<?php echo(get_option('link_linkedin') == false) ? "" : get_option('link_linkedin'); ?>" />
			</div>
		</div>

		<br>
		<input type="submit" name="save_social_info" class="button" value="Salvar Informações" />
	</div>
</form>