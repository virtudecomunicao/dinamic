<?php
/**
 * Dashboard Widget: Informações de Contato
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
defined('ABSPATH') OR exit('No direct script access allowed');
?>

<form action="" method="post">
	<div class="cmb_metabox">
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Telefone</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="phone" value="<?php echo(get_option('phone') == false) ? "" : get_option('phone'); ?>" />
			</div>
		</div>
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Celular</b></label>
			</div>
			<div class="field-item">
				<input type="text" class="input-4" name="celular" value="<?php echo(get_option('celular') == false) ? "" : get_option('celular'); ?>" />
			</div>
		</div>
		<div class="field CMB_Text_Field">
			<div class="field-title">
				<label><b>Endereço</b></label>
			</div>
			<div class="field-item">
				<textarea name="endereco" rows="4"><?php echo(get_option('endereco') == false) ? "" : get_option('endereco'); ?></textarea>
			</div>
		</div>
		
		<br>
		<input type="submit" name="save_info_contact" class="button" value="Salvar Informações" />
	</div>
</form>