<?php
/**
 * The template page: Contato
 *
 * @package WordPress
 * @subpackage Vihabile Arquitetura
 */
get_header(); ?>
<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
    <header style="background-image: url(<?php echo IMAGES; ?>/bg-contato.jpg);">
        <div id="mapa"></div>
    </header>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->

<main>
<section id="contato">
    <div class="title">
        <h2 class="verde contato"><?php echo get_option('endereco');?></h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col l6 m6 s12">
            <h3>Entre em contato conosco</h3>
            <?php echo do_shortcode('[contact-form-7 id="175" title="Contato"]'); ?>
            </div>
            <div class="col l6 m6 s12">
                <h3>Entre em contato e siga-nos!</h3>
                <ul>
                    <li><a href="mailto:<?php echo get_option('email');?>"><i class="fa fa-envelope-o"></i><span><?php echo get_option('email');?></span></a></li>
                    <li><a href="<?php echo get_option('link_facebook');?>" target="_blank"><i class="fa fa-facebook"></i><span><?php echo get_option('facebook');?></span></a></li>
                    <li><a href="<?php echo get_option('link_linkedin');?>" target="_blank"><i class="fa fa-linkedin"></i><span><?php echo get_option('linkedin');?></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

</main>

<?php get_footer(); ?>