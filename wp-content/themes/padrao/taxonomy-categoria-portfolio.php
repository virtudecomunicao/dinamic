<?php
/**
 * Page Template: Categoria Produtos
 *
 * @package WordPress
 * @subpackage Gustavo Nicolini
 */

get_header(); ?>

<!-- ||||||||||||||||||||||||  Slider |||||||||||||||||||||||||||||||||||||-->
    <header style="background-image: url(<?php echo IMAGES; ?>/bg-clientes.jpg);">
    
    </header>

<!-- ||||||||||||||||||||||||  CONTEÚDO  |||||||||||||||||||||||||||||||||||||-->

<main>
<section id="portfolio">
    <div class="title">
        <h2>Portfólio</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col l12 m12 s12 botoes">
                <a href="<?php echo site_url(); ?>/portfolios" class="button"><span>Todos</span></a>

                <?php 
                $args = array(
                    "number"     => "0",
                    "hide_empty" => "0"
                    );
                $terms = get_terms('categoria-portfolio',$args);
                $cont = 0;
                $url = $_SERVER['REQUEST_URI'];
                // echo $url;

                $urlEndereco = explode('/projetos/vihabile/dinamic/site/categoria-portfolio/', $url);

                $urlEndereco = explode('/', $urlEndereco[1]);


                ?>

                <?php foreach ($terms as $term) {
                    
                    $cat = $term->slug;?>
                    <a href="<?php echo get_term_link( $term->term_id,'categoria-portfolio');?>" class="button <?php if ($term->slug==$urlEndereco[0]) echo 'active';?>"><span><?php echo $term->name; ?></span></a>

                    <?php } ?>
            </div>
        </div>
        <div class="row">
            <?php

            if (have_posts() ) : while (have_posts() ) :the_post(); ?>
                <div class="col l4 m4 s6">
                    <a href="_portfolio-interna.html" class="port" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)"><h3 class="caption"><?php echo get_the_title();?></h3></a>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?> 

        </div>
    </div>
</section>

</main>

<?php get_footer(); ?>
